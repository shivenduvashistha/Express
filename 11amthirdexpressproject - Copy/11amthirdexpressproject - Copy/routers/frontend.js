const router = require("express").Router();
const Adminregc = require("../controllers/adminregcontroller");
const bannerc = require("../controllers/bannercontroller");
const queryc = require("../controllers/querycontroller");
const servicec = require("../controllers/servicecontroller");
const testic = require("../controllers/testicontroller");
const Contact = require("../models/contact");

const Banner = require("../models/banner");
const Services = require("../models/services");
const multer = require("multer");

let x = 5;

let storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./public/upload");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + file.originalname);
  },
});

let upload = multer({
  storage: storage,
  limits: { fileSize: 1024 * 1024 * 4 },
});

router.get('/abcd',(req,res)=>{
  res.send("abcd")
})
router.get("/", async (req, res) => {
  const bannerrecord = await Banner.findOne();
  const servicesrecord = await Services.find({ status: "publish" });
  const contactdata = await Contact.find();
  res.render("index.ejs", { bannerrecord, servicesrecord, contactdata });
});
router.get("/banner", bannerc.bannershow);
router.post("/queryrecords", queryc.querypostdata);
router.get("/servicedetail/:id", servicec.servicedetails);
router.get("/testi", testic.showtesti);
router.post("/testirecord", upload.single("img"), testic.testirecords);

module.exports = router;
