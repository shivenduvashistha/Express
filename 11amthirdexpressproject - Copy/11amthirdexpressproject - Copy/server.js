const express = require("express"); //function
const app = express(); //module
app.use(express.urlencoded({ extended: false }));
const frontendRouter = require("./routers/frontend");
const adminRouter = require("./routers/admin");
const mongoose = require("mongoose");
const session = require("express-session");
mongoose.connect("mongodb://127.0.0.1:27017/11amthirdexpressproject");

app.use(
  session({
    secret: "shiv",
    saveUninitialized: false,
    resave: false,
  })
);
app.use(express.static("public"));
app.use("/admin", adminRouter);
app.use(frontendRouter);
app.set("view engine", "ejs");
app.listen(5000, () => {
  console.log("server running at PORT 5000");
});
